import SpotData
import MLT_Analysis

sd = SpotData.SpotData('/mnt/alpha/work/PhD/DataArchive/sunspots/2014-09-05_16-FULL')
spot_list = sd.loadSpotData(sd.getDir('dat'))

spot = spot_list[0].history[9823]
roi = sd.loadROI(spot.ROI_path)
mlt = sd.loadMLT(spot.mlt_path)
layers = [x for x in mlt.layers if x.threshold_ratio in [0.55,0.35,0.2,0.15]]
MLT_Analysis.mlt_exploded_plot(roi, layers, xrange=[180,420], yrange=[180,420])

