# My libraries
import SpotTools
import SpotData
import MLT
import Contours

# Science libraries
from skimage.transform import hough_ellipse
from skimage.draw import ellipse_perimeter
import numpy as np
import matplotlib.pyplot as plt


def hough_comparison(plt, fig, roi, clusters):
    """Used to plot an ROI showing a sunspot onto a plt object."""
    # # Load ROI
    # try:
    #     roi = self.path_man.loadROI(snapshot.ROI_path)
    # except:
    #     return False

    # Plot background
    plt.imshow(roi.data, cmap='inferno', aspect='auto')
    roi_binary = np.zeros(np.array(roi.data).shape)

    # Plot contours for the given clusters
    for cluster in clusters:
        if len(cluster.points) < 100:
            # Reject because too small
            continue

        # Attempt at plotting only the perimeter
        perimeter = Contours.getPerimeter(cluster.points)
        # change perimeter from a list of [[x,y], ...] coords to a list of [[x],[y]] coords.
        perimeter = [list(x) for x in zip(*perimeter)]
        roi_binary[perimeter[0], perimeter[1]] = 255
        plt.scatter(perimeter[0], perimeter[1], c=['xkcd:blue'],
                    label=str(cluster.threshold_ratio) + r"$I_{quiet sun}$",
                    marker='s', s=(72. / fig.dpi) ** 2)

        # Getting Ellipse fit old method
        elli, xx, yy = MLT.MultiLevelThresholding.fit_ellipse(perimeter[0], perimeter[1])
        if elli is None:
            continue

        # get ellipse fit with Hough Transform
        # following example here: https://scikit-image.org/docs/dev/auto_examples/edges/plot_circular_elliptical_hough_transform.html#sphx-glr-auto-examples-edges-plot-circular-elliptical-hough-transform-py
        hough = hough_ellipse(roi_binary)
        hough.sort(order='accumulator')
        best_hough = list(hough[-1])
        yc_h, xc_h, a_h, b_h = [int(round(x)) for x in best_hough[1:5]]
        orientation_h = best_hough[5]
        hf_y, hf_x = ellipse_perimeter(yc_h, xc_h, a_h, b_h, orientation_h)

        # Record parameters and plot.
        # cluster.ellipse_parameters = elli
        # ellipse_colour = colourmap.get_rgb(cluster.threshold_ratio)
        # ellipse_colour = SpotTools.ensure_colour_bright(ellipse_colour)
        plt.plot(xx, yy, c='white', ms=(72. / fig.dpi))

        # Plot Hough ellipse
        plt.plot(hf_x, hf_y, c='xkcd:sea green', ms=(72. / fig.dpi))

    # Set up Contour plot axes
    plt.xlabel("Distance (pix)")
    plt.ylabel("Distance (pix)")
    return True


if __name__ == "__main__":
    sd = SpotData.SpotData('/home/rig12/Work/sunspots/2014-09-05_16-hourly')
    spot_list = sd.loadSpotData(sd.getDir('dat'))
    test_region = spot_list[0].history[100]
    roi = sd.loadROI(test_region.ROI_path)
    mlt = sd.loadMLT(test_region.mlt_path)
    clusters = mlt.find_layer_by_threshold(0.55).mlt_clusters

    fig = plt.figure(figsize=(16, 9), dpi=90)
    hough_comparison(plt, fig, roi, clusters)
    fig.tight_layout()
    plt.show()
