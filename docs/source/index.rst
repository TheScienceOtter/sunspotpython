.. SunspotMLT documentation master file, created by
   sphinx-quickstart on Mon Feb 10 15:26:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SunspotMLT's documentation!
======================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   ../../SunspotMain.py


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
