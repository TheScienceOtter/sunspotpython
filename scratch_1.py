import SpotData
import SpotTools
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as col
from matplotlib import cm

output_path = '/mnt/alpha/work/PhD/DataArchive/sunspots/2014-09-05_16-FULL/output/roi_diff'

sd = SpotData.SpotData('/mnt/alpha/work/PhD/DataArchive/sunspots/2014-09-05_16-FULL')
spot_list = sd.loadSpotData(sd.getDir('dat'))

start, stop = SpotTools.get_date_range_indices(spot_list[0], '2014-09-10_17-00-00', '2014-09-10_19-00-00')

c = np.fromfile('skewed_rainbow.cmp')
cmp = col.ListedColormap(c/255.0, name='skewed_rainbow')
norm = col.Normalize(vmin=3000,vmax=60000)
#cmap_norm = cm.ScalarMappable(norm=norm, cmap=cmp)

for i in range(start+1, stop):
    roi_1 = sd.loadROI(spot_list[0].history[i].timestamp.strftime('%Y-%m-%d_%H-%M-%S'))
    fig = plt.figure(figsize=(16,9), dpi=90)
    plt.imshow(roi_1.data, cmap=cmp, norm=norm)
    plt.colorbar()
    filename = spot_list[0].history[i].timestamp.strftime('%Y-%m-%d_%H-%M-%S')
    plt.title = filename
    plt.savefig(output_path + '/intensity_change/' + filename + '.png')
    plt.close()
